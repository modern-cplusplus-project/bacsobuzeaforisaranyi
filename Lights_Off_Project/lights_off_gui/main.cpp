#include <SFML/Graphics.hpp>
#include <iostream>
#include "WindowProprieties.h"
#include "Button.h"
#include "MainMenu.h"
#include "Timer.h"
#include "../Logging/Logging.h"
 
int main()
{
	Logging logger(std::cout);
	logger.Log("The app started running.", Logging::Level::INFO);
	MainMenu menu;
	menu.Menu();

	return 0;
}